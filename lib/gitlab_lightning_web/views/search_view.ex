defmodule GitlabLightningWeb.SearchView do
  use GitlabLightningWeb, :view

  def state(issue) do
    case issue.state do
      "opened" -> "(Open)"
      "closed" -> "(Closed)"
      _ -> "(Unknown)"
    end
  end

  def created_at(issue) do
    %{year: year, month: month, day: day} = issue.gitlab_created_at

    :io_lib.format("~4..0B-~2..0B-~2..0B", [year, month, day])
  end

  def author_handle(issue) do
    "@#{issue.author_username}"
  end
end
