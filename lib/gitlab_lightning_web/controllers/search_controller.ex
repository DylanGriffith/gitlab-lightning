defmodule GitlabLightningWeb.SearchController do
  use GitlabLightningWeb, :controller

  def show(conn, params) do
    query = params["q"]
    {:ok, issues} = GitlabLightning.Searcher.search(query)

    render conn, "show.html", query: query, issues: issues
  end
end
