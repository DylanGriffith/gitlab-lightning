defmodule GitlabLightningWeb.PageController do
  use GitlabLightningWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
