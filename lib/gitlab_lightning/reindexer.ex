defmodule GitlabLightning.Reindexer do
  alias GitlabLightning.Repo
  import Ecto.Query

  @indexer GitlabLightning.Indexer

  def reindex(indexer \\ @indexer) do
    syncs = Repo.all(GitlabLightning.IssueSync)

    syncs |> Enum.each(fn(sync) ->
      :ok = indexer.index_sync(sync.gitlab_host, sync.project_path, sync.label)
    end)

    :ok
  end
end
