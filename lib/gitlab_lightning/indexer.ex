defmodule GitlabLightning.Indexer do
  alias GitlabLightning.Repo
  import Ecto.Query

  @client GitlabLightning.Gitlab.Client

  def index_sync(gitlab_host, project_path, label, client \\ @client) do
    issues = client.get_issues(gitlab_host, project_path, opts(gitlab_host, project_path, label))

    issues |> Enum.each(fn(issue) ->
      Repo.insert!(%GitlabLightning.Gitlab.Issue{
        gitlab_id: issue["id"],
        gitlab_iid: issue["iid"],
        gitlab_url: issue["web_url"],
        title: issue["title"],
        description: issue["description"],
        state: issue["state"],
        gitlab_created_at: NaiveDateTime.from_iso8601!(issue["created_at"]),
        gitlab_updated_at: NaiveDateTime.from_iso8601!(issue["updated_at"]),
        author_username: issue["author"]["username"]
      }, on_conflict: :replace_all, conflict_target: :gitlab_id)
    end)

    update_last_synced!(gitlab_host, project_path, label)

    :ok
  end

  defp update_last_synced!(gitlab_host, project_path, label) do
    now = NaiveDateTime.utc_now
    Repo.insert!(%GitlabLightning.IssueSync{
      gitlab_host: gitlab_host,
      project_path: project_path,
      label: label,
      last_synced: now,
    }, on_conflict: [set: [last_synced: now]], conflict_target: [:gitlab_host, :project_path, :label])
  end

  defp opts(gitlab_host, project_path, label) do
    search = Repo.get_by(
      GitlabLightning.IssueSync,
      gitlab_host: gitlab_host,
      project_path: project_path,
      label: label,
    )

    opts = [labels: label]

    if search do
      opts ++ [updated_after: search.last_synced]
    else
      opts
    end
  end
end
