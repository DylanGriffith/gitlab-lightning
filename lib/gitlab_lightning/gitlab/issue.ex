defmodule GitlabLightning.Gitlab.Issue do
  use Ecto.Schema
  import Ecto.Changeset


  schema "gitlab_issues" do
    field :gitlab_id, :integer
    field :gitlab_iid, :integer
    field :gitlab_url, :string
    field :title, :string
    field :description, :string
    field :state, :string
    field :gitlab_created_at, :naive_datetime
    field :gitlab_updated_at, :naive_datetime
    field :author_username, :string

    timestamps()
  end
end
