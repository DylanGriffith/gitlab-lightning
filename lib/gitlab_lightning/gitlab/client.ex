defmodule GitlabLightning.Gitlab.Client do

  @http_client HTTPoison

  def get_issues(host, project_path, opts, http_client \\ @http_client) do
    {results, total_pages} = get_issues_for_page(host, project_path, opts, http_client, 1)

    if total_pages > 1 do
      results ++ (
        2..total_pages |>
        Enum.map(fn(page) ->
          Task.async(fn() -> get_issues_for_page(host, project_path, opts, http_client, page) end)
        end) |>
        Enum.flat_map(fn(task) ->
          {results, _} = Task.await(task)
          results
        end)
      )
    else
      results
    end
  end

  defp get_issues_for_page(host, project_path, opts, http_client, page) do
    query = [order_by: :updated_at, sort: :desc, page: page]
    path = "/api/v4/projects/#{URI.encode_www_form(project_path)}/issues"
    uri = URI.parse(host)
    uri = %URI{
      uri |
      path: path,
      query: URI.encode_query(Keyword.merge(query, opts)),
    }

    %HTTPoison.Response{status_code: 200, headers: headers, body: body} =
      http_client.get!(URI.to_string(uri))

    results = Poison.decode!(body)

    {_, total_pages} = Enum.find(headers, fn({k, _}) -> k == "X-Total-Pages" end)

    {total_pages, ""} = Integer.parse(total_pages)
    {results, total_pages}
  end
end
