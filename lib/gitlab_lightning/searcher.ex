defmodule GitlabLightning.Searcher do
  import Ecto.Query

  @search_column "(setweight(to_tsvector('english', title), 'A') || setweight(to_tsvector('english', description), 'B'))"
  @text_search "#{@search_column} @@ websearch_to_tsquery(?)"
  @rank_search "ts_rank(#{@search_column}, websearch_to_tsquery(?))"

  def search(search) do
    query =
      from i in GitlabLightning.Gitlab.Issue,
      where: fragment(@text_search, ^search),
      order_by: [desc: i.state, desc: fragment(@rank_search, ^search), desc: i.gitlab_updated_at],
      limit: 20

    issues = GitlabLightning.Repo.all(query)
    {:ok, issues}
  end
end
