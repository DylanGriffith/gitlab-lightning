defmodule GitlabLightning.ReindexScheduler do
  use GenServer

  @five_minutes 5 * 60 * 1000

  def start_link do
    GenServer.start_link(__MODULE__, name: __MODULE__)
  end

  def init(state) do
    schedule()
    {:ok, state}
  end

  def handle_info(:go, state) do
    GitlabLightning.Reindexer.reindex

    schedule()

    {:noreply, state}
  end

  defp schedule do
    Process.send_after(self(), :go, @five_minutes)
  end
end
