defmodule GitlabLightning.IssueSync do
  use Ecto.Schema
  import Ecto.Changeset

  schema "issue_syncs" do
    field :gitlab_host, :string
    field :label, :string
    field :last_synced, :naive_datetime
    field :project_path, :string
  end
end
