defmodule GitlabLightning.Application do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec

    children = [
      supervisor(GitlabLightning.Repo, []),
      supervisor(GitlabLightningWeb.Endpoint, []),
      worker(GitlabLightning.ReindexScheduler, []),
    ]

    opts = [strategy: :one_for_one, name: GitlabLightning.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def config_change(changed, _new, removed) do
    GitlabLightningWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
