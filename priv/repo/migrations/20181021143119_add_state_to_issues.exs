defmodule GitlabLightning.Repo.Migrations.AddStateToIssues do
  use Ecto.Migration

  def change do
    alter table("gitlab_issues") do
      add :state, :text, null: false
    end
  end
end
