defmodule GitlabLightning.Repo.Migrations.CreateIssueSyncs do
  use Ecto.Migration

  def change do
    create table(:issue_syncs) do
      add :gitlab_host, :string, null: false
      add :project_path, :string, null: false
      add :label, :string, null: false
      add :last_synced, :naive_datetime, null: false
    end

    create index(:issue_syncs, [:gitlab_host, :project_path, :label], unique: true)
  end
end
