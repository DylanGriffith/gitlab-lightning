defmodule GitlabLightning.Repo.Migrations.CreateGitlabIssues do
  use Ecto.Migration

  def change do
    create table(:gitlab_issues) do
      add :gitlab_id, :bigint
      add :gitlab_iid, :bigint
      add :gitlab_url, :string
      add :title, :string
      add :gitlab_created_at, :naive_datetime
      add :gitlab_updated_at, :naive_datetime

      timestamps()
    end

  end
end
