defmodule GitlabLightning.Repo.Migrations.AddAuthorToIssues do
  use Ecto.Migration

  def up do
    alter table("gitlab_issues") do
      add :author_username, :text
    end

    # Backfill old/bad usernames
    execute "UPDATE gitlab_issues SET author_username = 'missing'"

    # Clear the last_synced so we refresh all data
    execute "UPDATE issue_syncs SET last_synced = '2000-01-01'::timestamp"

    alter table("gitlab_issues") do
      modify :author_username, :text, null: false
    end
  end

  def down do
    alter table("gitlab_issues") do
      remove :author_username
    end
  end
end
