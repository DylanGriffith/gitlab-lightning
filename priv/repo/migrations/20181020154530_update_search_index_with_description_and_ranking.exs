defmodule GitlabLightning.Repo.Migrations.UpdateSearchIndexWithDescriptionAndRanking do
  use Ecto.Migration

  def up do
    drop index(:gitlab_issues, [], name: "issue_search_index")

    create index(:gitlab_issues, ["(setweight(to_tsvector('english', title), 'A') || setweight(to_tsvector('english', description), 'B'))"], [
      name: "issue_search_index",
      using: "GIN"
    ])
  end

  def down do
    drop index(:gitlab_issues, [], name: "issue_search_index")

    create index(:gitlab_issues, ["to_tsvector('english', title)"], [
      name: "issue_search_index",
      using: "GIN"
    ])
  end
end
