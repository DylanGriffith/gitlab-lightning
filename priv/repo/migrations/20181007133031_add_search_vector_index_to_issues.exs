defmodule GitlabLightning.Repo.Migrations.AddSearchVectorIndexToIssues do
  use Ecto.Migration

  def up do
    create index(:gitlab_issues, ["to_tsvector('english', title)"], [
      name: "issue_search_index",
      using: "GIN"
    ])
  end
end
