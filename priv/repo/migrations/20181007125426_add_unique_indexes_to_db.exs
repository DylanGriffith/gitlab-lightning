defmodule GitlabLightning.Repo.Migrations.AddUniqueIndexesToDb do
  use Ecto.Migration

  def change do
    create index("gitlab_issues", [:gitlab_id], unique: true)
  end
end
