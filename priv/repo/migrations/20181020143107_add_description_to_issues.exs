defmodule GitlabLightning.Repo.Migrations.AddDescriptionToIssues do
  use Ecto.Migration

  def change do
    alter table("gitlab_issues") do
      add :description, :text, null: false, default: ""
    end
  end
end
