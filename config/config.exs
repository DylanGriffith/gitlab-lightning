# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :gitlab_lightning,
  ecto_repos: [GitlabLightning.Repo]

# Configures the endpoint
config :gitlab_lightning, GitlabLightningWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "yAmPq7roiZmtodqRTR7Tj4sMWOdKw/T6wnXRvM3NVsDnnzPYyB/64IOfDqYieinh",
  render_errors: [view: GitlabLightningWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: GitlabLightning.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
