use Mix.Config

config :gitlab_lightning, GitlabLightningWeb.Endpoint,
  http: [port: 4001],
  server: true

config :gitlab_lightning, :sql_sandbox, true

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :gitlab_lightning, GitlabLightning.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  database: System.get_env("POSTGRES_DB") || "gitlab_lightning_test",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

config :wallaby,
  driver: Wallaby.Experimental.Chrome
