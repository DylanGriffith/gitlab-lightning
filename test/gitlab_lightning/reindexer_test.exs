defmodule GitlabLightning.ReindexerTest do
  use GitlabLightning.DataCase

  defmodule MockIndexer do
    def index_sync(gitlab_host, project_path, label) do
      send self(), %{gitlab_host: gitlab_host, project_path: project_path, label: label}
      :ok
    end
  end

  @sync1 %GitlabLightning.IssueSync{
    gitlab_host: "https://gitlab.example.com/",
    label: "SomeLabel",
    last_synced: ~N[2018-03-21 12:00:00.000000],
    project_path: "/my-group/my-proj",
  }

  @sync2 %GitlabLightning.IssueSync{
    gitlab_host: "https://gitlab.example.com/",
    label: "SomeOtherLabel",
    last_synced: ~N[2018-03-22 12:00:00.000000],
    project_path: "/my-group/my-other-proj",
  }

  setup do
    Repo.insert!(@sync1)
    Repo.insert!(@sync2)
    :ok
  end

  test "#reindex - finds previous syncs and calls index_sync for them" do
    :ok = GitlabLightning.Reindexer.reindex(MockIndexer)

    assert_received %{
      gitlab_host: "https://gitlab.example.com/",
      label: "SomeLabel",
      project_path: "/my-group/my-proj",
    }

    assert_received %{
      gitlab_host: "https://gitlab.example.com/",
      label: "SomeOtherLabel",
      project_path: "/my-group/my-other-proj",
    }
  end
end
