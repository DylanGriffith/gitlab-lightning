defmodule GitlabLightning.IndexerTest do
  use GitlabLightning.DataCase

  defmodule Fake.GitlabLightning.Gitlab.Client do
    def get_issues("https://my-gitlab.example.com", "my-group/my-project", [labels: "MyLabel"])  do
      [
        %{
          "id" => 123,
          "iid" => 11,
          "web_url" => "http://my-gitlab.example.com/my-group/my-project/issues/11",
          "title" => "The first issue",
          "description" => "The description of the first issue",
          "state" => "opened",
          "created_at" => "2018-03-20T12:00:00Z",
          "updated_at" => "2018-03-21T14:00:00Z",
          "author" => %{
            "username" => "alice",
          },
        },
        %{
          "id" => 456,
          "iid" => 22,
          "web_url" => "http://my-gitlab.example.com/my-group/my-project/issues/22",
          "title" => "The second issue",
          "description" => "The description of the second issue",
          "state" => "closed",
          "created_at" => "2018-04-20T13:00:00Z",
          "updated_at" => "2018-04-21T15:00:00Z",
          "author" => %{
            "username" => "bob",
          },
        },
      ]
    end

    def get_issues("https://my-gitlab.example.com", "my-group/my-project", [labels: "MyLabel", updated_after: _])  do
      [
        %{
          "id" => 789,
          "iid" => 33,
          "web_url" => "http://my-gitlab.example.com/my-group/my-project/issues/33",
          "title" => "The third issue",
          "description" => "The description of the third issue",
          "state" => "closed",
          "created_at" => "2018-04-20T13:00:00Z",
          "updated_at" => "2018-04-21T15:00:00Z",
          "author" => %{
            "username" => "charles",
          },
        },
      ]
    end
  end

  test "#index_sync - calls GitlabLighting.Gitlab.Client.get_issues and persists them" do
    :ok = GitlabLightning.Indexer.index_sync("https://my-gitlab.example.com", "my-group/my-project", "MyLabel", Fake.GitlabLightning.Gitlab.Client)

    issues = GitlabLightning.Gitlab.Issue |> Repo.all

    assert length(issues) == 2

    [issue1, issue2] = issues

    assert issue1.gitlab_id == 123
    assert issue1.gitlab_iid == 11
    assert issue1.gitlab_url == "http://my-gitlab.example.com/my-group/my-project/issues/11"
    assert issue1.title == "The first issue"
    assert issue1.description == "The description of the first issue"
    assert issue1.state == "opened"
    assert issue1.gitlab_created_at == ~N[2018-03-20 12:00:00.000000]
    assert issue1.gitlab_updated_at == ~N[2018-03-21 14:00:00.000000]
    assert issue1.author_username == "alice"

    assert issue2.gitlab_id == 456
    assert issue2.gitlab_iid == 22
    assert issue2.gitlab_url == "http://my-gitlab.example.com/my-group/my-project/issues/22"
    assert issue2.title == "The second issue"
    assert issue2.description == "The description of the second issue"
    assert issue2.state == "closed"
    assert issue2.gitlab_created_at == ~N[2018-04-20 13:00:00.000000]
    assert issue2.gitlab_updated_at == ~N[2018-04-21 15:00:00.000000]
    assert issue2.author_username == "bob"
  end

  test "#index_sync - when called again for the same search passes updated_after" do
    :ok = GitlabLightning.Indexer.index_sync("https://my-gitlab.example.com", "my-group/my-project", "MyLabel", Fake.GitlabLightning.Gitlab.Client)

    :ok = GitlabLightning.Indexer.index_sync("https://my-gitlab.example.com", "my-group/my-project", "MyLabel", Fake.GitlabLightning.Gitlab.Client)

    issues = GitlabLightning.Gitlab.Issue |> Repo.all

    assert length(issues) == 3

    [_, _, issue3] = issues

    assert issue3.gitlab_id == 789
  end
end
