defmodule GitlabLightning.SearcherTest do
  use GitlabLightning.DataCase

  @issue1 %GitlabLightning.Gitlab.Issue{
        gitlab_id: 11,
        gitlab_iid: 1,
        gitlab_url: "https://gitlab.example.com/my-group/my-project/issues/1",
        title: "The first title",
        description: "This issue is about cats",
        state: "opened",
        gitlab_created_at: ~N[2018-03-21 12:00:00.000000],
        gitlab_updated_at: ~N[2018-03-22 14:00:00.000000],
        author_username: "alice",
      }

  @issue2 %GitlabLightning.Gitlab.Issue{
        gitlab_id: 12,
        gitlab_iid: 2,
        gitlab_url: "https://gitlab.example.com/my-group/my-project/issues/2",
        title: "The second title",
        description: "This issue is about dogs",
        state: "opened",
        gitlab_created_at: ~N[2018-03-23 12:00:00.000000],
        gitlab_updated_at: ~N[2018-03-24 14:00:00.000000],
        author_username: "alice",
      }

  @issue3 %GitlabLightning.Gitlab.Issue{
        gitlab_id: 13,
        gitlab_iid: 3,
        gitlab_url: "https://gitlab.example.com/my-group/my-project/issues/3",
        title: "The third title",
        description: "This issue is about frogs",
        state: "opened",
        gitlab_created_at: ~N[2018-03-25 12:00:00.000000],
        gitlab_updated_at: ~N[2018-03-26 14:00:00.000000],
        author_username: "alice",
      }

  @issue4 %GitlabLightning.Gitlab.Issue{
        gitlab_id: 14,
        gitlab_iid: 4,
        gitlab_url: "https://gitlab.example.com/my-group/my-project/issues/4",
        title: "The fourth title is about frogs",
        description: "This issue is has a well written title",
        state: "opened",
        gitlab_created_at: ~N[2018-03-20 12:00:00.000000],
        gitlab_updated_at: ~N[2018-03-21 14:00:00.000000],
        author_username: "alice",
      }

  setup do
    Repo.insert!(@issue1)
    Repo.insert!(@issue2)
    Repo.insert!(@issue3)
    Repo.insert!(@issue4)
    :ok
  end

  test "#search" do
    {:ok, [issue]} = GitlabLightning.Searcher.search("second")

    assert issue.title == "The second title"
  end

  test "#search - with boolean logic" do
    {:ok, [issue2, issue1]} = GitlabLightning.Searcher.search("first OR second")

    assert issue1.title == "The first title"
    assert issue2.title == "The second title"
  end

  test "#search - with negation" do
    {:ok, [issue3]} = GitlabLightning.Searcher.search("frogs -fourth")

    assert issue3.title == "The third title"
  end

  test "#search - with spaces" do
    {:ok, [issue1]} = GitlabLightning.Searcher.search("about cats")

    assert issue1.title == "The first title"
  end

  test "#search - for issue description" do
    {:ok, [issue2]} = GitlabLightning.Searcher.search("dogs")

    assert issue2.title == "The second title"
    assert issue2.description == "This issue is about dogs"
  end

  test "#search - orders by title match first" do
    {:ok, [issue4, issue3]} = GitlabLightning.Searcher.search("frogs")

    assert issue4.title == "The fourth title is about frogs"
    assert issue4.description == "This issue is has a well written title"

    assert issue3.title == "The third title"
    assert issue3.description == "This issue is about frogs"
  end

  test "#search - is limited to first 20 matches" do
    1..25
    |> Enum.each(fn(i) ->
      Repo.insert!(%{ @issue1 | gitlab_id: i * 10})
    end)

    {:ok, issues} = GitlabLightning.Searcher.search("cats")

    assert Enum.count(issues) == 20
  end
end
