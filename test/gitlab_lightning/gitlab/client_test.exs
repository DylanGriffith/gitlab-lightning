defmodule GitlabLightning.Gitlab.ClientTest do
  use ExUnit.Case
  alias GitlabLightning.Gitlab.Client

  @response1 [
    %{ "id" => 111, "iid" => 1, "web_url" => "http://my-gitlab.example.com/my-group/my-project/issues/1", "title" => "The first issue", "created_at" => "2018-03-20T12:00:00Z", "updated_at" => "2018-03-21T14:00:00Z" },
    %{ "id" => 222, "iid" => 2, "web_url" => "http://my-gitlab.example.com/my-group/my-project/issues/2", "title" => "The second issue", "created_at" => "2018-04-20T13:00:00Z", "updated_at" => "2018-04-21T15:00:00Z" },
  ]
  def response1, do: @response1

  @response2 [
    %{ "id" => 333, "iid" => 3, "web_url" => "http://my-gitlab.example.com/my-group/my-project/issues/3", "title" => "The third issue", "created_at" => "2018-03-20T12:00:00Z", "updated_at" => "2018-03-21T14:00:00Z" },
    %{ "id" => 444, "iid" => 4, "web_url" => "http://my-gitlab.example.com/my-group/my-project/issues/4", "title" => "The fourth issue", "created_at" => "2018-04-20T13:00:00Z", "updated_at" => "2018-04-21T15:00:00Z" },
  ]
  def response2, do: @response2

  @response3 [
    %{ "id" => 555, "iid" => 5, "web_url" => "http://my-gitlab.example.com/my-group/my-project/issues/5", "title" => "The fifth issue", "created_at" => "2018-03-20T12:00:00Z", "updated_at" => "2018-03-21T14:00:00Z" },
    %{ "id" => 666, "iid" => 6, "web_url" => "http://my-gitlab.example.com/my-group/my-project/issues/6", "title" => "The sixth issue", "created_at" => "2018-04-20T13:00:00Z", "updated_at" => "2018-04-21T15:00:00Z" },
  ]
  def response3, do: @response3

  defmodule Fake.HTTPoison do
    def get!("https://my-gitlab.example.com/api/v4/projects/my-group%2Fmy-project/issues?order_by=updated_at&sort=desc&page=1&labels=MyLabel")  do
      %HTTPoison.Response{status_code: 200,
        headers: [{"content-type", "application/json"}, {"X-Total-Pages", "3"}],
        body: Poison.encode!(GitlabLightning.Gitlab.ClientTest.response1)}
    end

    def get!("https://my-gitlab.example.com/api/v4/projects/my-group%2Fmy-project/issues?order_by=updated_at&sort=desc&page=2&labels=MyLabel")  do
      %HTTPoison.Response{status_code: 200,
        headers: [{"content-type", "application/json"}, {"X-Total-Pages", "3"}],
        body: Poison.encode!(GitlabLightning.Gitlab.ClientTest.response2)}
    end

    def get!("https://my-gitlab.example.com/api/v4/projects/my-group%2Fmy-project/issues?order_by=updated_at&sort=desc&page=3&labels=MyLabel")  do
      %HTTPoison.Response{status_code: 200,
        headers: [{"content-type", "application/json"}, {"X-Total-Pages", "3"}],
        body: Poison.encode!(GitlabLightning.Gitlab.ClientTest.response3)}
    end
  end

  test "it makes http request and parses json" do
    result = Client.get_issues("https://my-gitlab.example.com", "my-group/my-project", [labels: "MyLabel"], Fake.HTTPoison)
    assert result == @response1 ++ @response2 ++ @response3
  end
end
