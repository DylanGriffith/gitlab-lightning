defmodule GitlabLightning.Features.IssueTitleSearchrest do
  use GitlabLightning.FeatureCase, async: true

  import Wallaby.Query, only: [css: 2, text_field: 1, button: 1]

  test "users can search issue titles", %{session: session} do
    :ok = GitlabLightning.Indexer.index_sync("https://gitlab.com", "DylanGriffith/gitlab-lightning", "TestData")

    session
    |> visit("/")
    |> fill_in(text_field("Search"), with: "pop")
    |> click(button("Search"))
    |> assert_has(css(".issue-title", text: "Make it pop"))
    |> assert_has(css(".issue-link", href: "https://gitlab.com/DylanGriffith/gitlab-lightning/issues/2"))
    |> assert_has(css(".issue-description", text: "It's good but I just want it to pop more."))
    |> assert_has(css(".issue-state", text: "(Open)"))
    |> assert_has(css(".issue-created-at", text: "2018-09-22"))
    |> assert_has(css(".issue-created-by", text: "@DylanGriffith"))
  end
end
