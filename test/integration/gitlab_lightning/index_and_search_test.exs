defmodule GitlabLightning.IndexAndSearchTest do
  use GitlabLightning.DataCase

  test "index and scan" do
    :ok = GitlabLightning.Indexer.index_sync("https://gitlab.com", "DylanGriffith/gitlab-lightning", "TestData")

    {:ok, [issue]} = GitlabLightning.Searcher.search("pop")

    assert issue.title == "Make it pop"
    assert issue.gitlab_created_at.day == 22
    assert issue.gitlab_updated_at.day == 23
  end
end
