defmodule GitlabLightning.FeatureCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      @tag :feature
      use Wallaby.DSL

      alias GitlabLightning.Repo
      import Ecto
      import Ecto.Changeset
      import Ecto.Query

      import GitlabLightningWeb.Router.Helpers
    end
  end

  setup tags do
    {:ok, _} = Application.ensure_all_started(:wallaby)

    Application.put_env(:wallaby, :base_url, GitlabLightningWeb.Endpoint.url)

    :ok = Ecto.Adapters.SQL.Sandbox.checkout(GitlabLightning.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(GitlabLightning.Repo, {:shared, self()})
    end

    metadata = Phoenix.Ecto.SQL.Sandbox.metadata_for(GitlabLightning.Repo, self())
    {:ok, session} = Wallaby.start_session(metadata: metadata)
    {:ok, session: session}
  end
end
