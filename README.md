# GitlabLightning

This is intended to be a tool I run locally to give me fast offline access
to all of the things I regularly do on gitlab.com .

## Running Locally

### Requirements

 - Elixir ~> 1.4
 - Postgresql >= 11.0

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Fetch and index some data

From iex (`iex -S mix`) run:

```elixir
:ok = GitlabLightning.Indexer.index_sync("https://gitlab.com", "gitlab-org/gitlab-ce", "Configure")
:ok = GitlabLightning.Indexer.index_sync("https://gitlab.com", "gitlab-org/gitlab-ee", "Configure")
:ok = GitlabLightning.Indexer.index_sync("https://gitlab.com", "gitlab-org/gitlab-ce", "auto devops")
:ok = GitlabLightning.Indexer.index_sync("https://gitlab.com", "gitlab-org/gitlab-ee", "auto devops")
```

## Run The Tests

```bash
mix test --include feature
```

## TODO


- [X] Search by issue title
- [X] Add unique indexes in DB
- [X] Make indexer UPSERT issues
- [X] Actually use postgres full text search
- [X] Add an index for full text search
- [X] Present the issues in a nicer UI
- [X] Update client to get all pages of issues
- [X] CI
- [X] Copy issue URL to clipboard 1 click
- [X] Search by issue description
- [X] Update index to include issue description
- [X] Rank by title match first
- [X] Fetch issues concurrently
- [X] Display if the issue is closed
- [X] Limit results to first 20
- [X] Sort by state first
- [X] Add `updated_after` param to issues request
- [X] Display issue created at
- [X] Display issue author
- [X] Re-sync issues automatically on schedule
- [X] CD with Auto DevOps
- [ ] Chrome extension that tracks every page you visit and makes sure it is added to the cache
- [ ] Search for merge requests by title and description
- [ ] Search for epics by title and description
- [ ] Search by issue comment
- [ ] Copy comment URL to clipboard 1 click
- [ ] Search by issue creator
- [ ] Search by comment author
- [ ] Sort by comment created date
- [ ] Search by label
- [ ] Run feature (wallaby) tests in CI
- [ ] Allow show/hide issue description
- [ ] Auto show issue description if this is the text that matches
- [ ] Highlight the matching text

## Deployment

This can be deployed by Auto DevOps.
You will need the following set up:

### Cluster

You will need a cluster connected to your project with the following installed:

- Ingress
- Cert Manager

### CI/CD Variables

- ADDITIONAL_HOSTS=lightning.dylangriffith.net # set for any domain you'd like, scoped to production environment
- DB_INITIALIZE='sleep 30 && /bin/herokuish procfile exec mix ecto.create'
- DB_MIGRATE='/bin/herokuish procfile exec mix ecto.migrate'
- BUILDPACK_URL=https://github.com/HashNuke/heroku-buildpack-elixir

### Caveates/Workarounds

- We are using a randomly generated `secret_key_base` in `config/prod.exs` since there is no way to pass environment variables into the compile stage of Auto DevOps (https://gitlab.com/gitlab-org/gitlab-ce/issues/54681) . Using a random secret here probably means that probably you will not be able to rely on using the session cookie at all since it will be different on every pod running the app.
- The host has been hardcoded in `config/prod.exs` which means it will be the same for all deployed environments (staging, review, production). Again, since environment variables can't be passed in during compile time it doesn't seem easy to make this dynamic or dependent on the environment.
- Selenium tests won't pass so they need to be disabled by default with `mix test`
